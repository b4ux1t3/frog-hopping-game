using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = Unity.Mathematics.Random;

public class SpawnerController : MonoBehaviour
{
    public List<GameObject> spawnables;
    public Vector3 travelDirection;
    public SpawnerPool pool;
    public float spawnInterval;
    public float spawnWindow;
    public float moveSpeed = 5;
    public RandomSource rs;
    private string[] _spawnableNames;
    private float _spawnTimer;
    private void OnEnable()
    {
        
    }

    // Start is called before the first frame update
    private void Start()
    {
        _spawnableNames = new string[spawnables.Count];
        for (int i = 0; i < spawnables.Count; i++)
        {
            _spawnableNames[i] = spawnables[i].GetComponent<Spawnable>().spawnableName;
        }

        _spawnTimer = Time.time + rs.random.NextFloat(spawnInterval - spawnWindow, spawnInterval + spawnWindow);
    }

    // Update is called once per frame
    private void Update()
    {
        if (Time.time < _spawnTimer) return;
        SpawnInstance();
    }

    /// <summary>
    /// Gets a <see cref="Spawnable"/> object from the <see cref="pool"/>.
    /// </summary>
    private void SpawnInstance(){
        var newSpawnName = _spawnableNames[rs.random.NextInt(0, _spawnableNames.Length)];
        var newSpawn = pool.GetSpawnable(newSpawnName, transform);
        StartSpawnable(newSpawn);
        _spawnTimer = Time.time + rs.random.NextFloat(spawnInterval - spawnWindow, spawnInterval + spawnWindow);
    }
    
    /// <summary>
    /// Sets the start position and direction of a spawned object.
    /// </summary>
    /// <param name="spawnableInstance">An instance of one of this class's spawnables.</param>
    /// <exception cref="InvalidGameObjectException">
    /// Throws if this <c>GameObject</c> has no <c>Spawnable</c> component.
    /// </exception>
    private void StartSpawnable(GameObject spawnableInstance)
    {
        spawnableInstance.transform.position = transform.position;
        var spawnable = spawnableInstance.GetComponent<Spawnable>();
        var moveController = spawnableInstance.GetComponent<MovementController>();
        moveController.hopTime = moveSpeed;
        if (spawnable == null) throw new InvalidGameObjectException("GameObject is not a spawnable!");
        spawnable.movementDirection = travelDirection;
        spawnableInstance.SetActive(true);
    }

    private class InvalidGameObjectException : Exception
    {
        public InvalidGameObjectException(string message) : base(message)
        {
            
        }
    }
}
