using System.Collections.Generic;
using UnityEngine;

public class SpawnerPool : MonoBehaviour
{
    public List<GameObject> spawnables;
    private Dictionary<string, List<GameObject>> _spawnablePool;
    
    public GameObject GetSpawnable(string spawnableName, Transform parent)
    {
        if (!_spawnablePool.ContainsKey(spawnableName)) return GenerateSpawnable(spawnableName, parent);
        var matches = _spawnablePool[spawnableName].FindAll(o => o.activeSelf == false);
        return matches.Count < 1 ? GenerateSpawnable(spawnableName, parent) : matches[0].gameObject;
    }
    private void AddSpawnable(GameObject usedSpawnable)
    {
        var spawnableName = usedSpawnable.GetComponent<Spawnable>().spawnableName;
        if (_spawnablePool.ContainsKey(spawnableName))
        {
            _spawnablePool[spawnableName].Add(usedSpawnable);
        }
        else
        {
            _spawnablePool.Add(spawnableName, new List<GameObject>());
            // ReSharper disable once TailRecursiveCall
            AddSpawnable(usedSpawnable); // We're only ever going to be able to recurse once
        }
    }

    private GameObject GenerateSpawnable(string spawnableName, Transform parent)
    {
        var spawnPrefab = spawnables.Find((go) =>
        {
            var goSpawnableName = go.GetComponent<Spawnable>().spawnableName;
            return goSpawnableName == spawnableName;
        });
        var newSpawn = Instantiate(spawnPrefab, parent.position, Quaternion.identity);
        AddSpawnable(newSpawn);
        return newSpawn;
    }

    private void Start()
    {
        _spawnablePool = new Dictionary<string, List<GameObject>>();
    }
}
