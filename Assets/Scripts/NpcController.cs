using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class NpcController : MonoBehaviour
{
    public MovementController movementController;
    public string endTag;
    public bool manuallySpawned = false;
    private Spawnable _spawnable;

    private void OnEnable()
    {
        if (manuallySpawned) return;
        _spawnable = gameObject.GetComponent<Spawnable>();
        movementController.moveSpeed = _spawnable.movementDirection;
    }

    // Update is called once per frame
    private void Update()
    {
        if (!gameObject.activeSelf) return;
        movementController.Move(_spawnable.movementDirection);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(endTag))
        {
            gameObject.SetActive(false);
        }
    }
}
