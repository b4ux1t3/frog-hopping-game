using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Serialization;

/// <summary>
/// The logic for controlling our player. Handles things like, for example, keyboard input.
///
/// There should be a different script for the actual movement, this just passes input values to the movement controller
/// </summary>
public class PlayerController : MonoBehaviour
{
    public string enemyTag;
    public string riverTag;
    public string logTag;
    public string barrierTag;
    public string endTag;
    public TMP_Text WinLoseText;
    
    [SerializeField] private MovementController movementController;
    private InputMain _input;
    private bool _onRiver;
    private GameObject _log;
    private bool _moving;
    private float _logOffset;
    private bool _win;
    private void Awake()
    {
        _log = null;
        _onRiver = false;
        _input = new InputMain();
        _win = false;
        _input.Player.Movement.performed += (ctx) =>
        {
            //TODO: See the TODO in MovementController.
            var vec = ctx.ReadValue<Vector2>();
            if (vec.magnitude == 0) return;
            _moving = true;
            if (vec.x < 0) movementController.Left(); else if (vec.x > 0) movementController.Right();
            if (vec.y < 0) movementController.Down(); else if (vec.y > 0) movementController.Up();
        };
    }

    private void OnEnable()
    {
        _input.Enable();
    }

    private void OnDisable()
    {
        _input.Disable();
    }

    private void Update()
    {
        if (_log != null && !_moving)
        {
            var logPosition = _log.transform.position;
            transform.position =
                new Vector3(logPosition.x - _logOffset, transform.position.y, transform.position.z);
        }
        if (_moving) return;
        CheckDeath();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag(barrierTag))
        {
            if (_onRiver)
            {
                EndGame(false);
                return;
            }

            if (_moving) movementController.ResetPos();
        }
        
        if (other.gameObject.CompareTag(enemyTag))
        {
            EndGame(false);
        }

        if (other.gameObject.CompareTag(logTag))
        {
            _log = other.gameObject;
            if (!_moving) _logOffset = _log.transform.position.x - transform.position.x;
        }

        _win = _win || other.gameObject.CompareTag(endTag);
        _onRiver = _onRiver || other.gameObject.CompareTag(riverTag);
    }
    
    /// <summary>
    /// When we jump from one log to another, we will collide with another collider, triggering
    /// <see cref="OnTriggerEnter2D"/>. That will update our "current log" to be that new log.
    /// If the log we're leaving isn't teh same as our current log, then there's no reason to
    /// nullify our current log. 
    /// </summary>
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag(riverTag)) _onRiver = false;
        if (other.CompareTag(endTag)) _win = false;
        if (!other.gameObject.CompareTag(logTag)) return;
        if (other.gameObject.GetInstanceID() == _log.GetInstanceID()) _log = null;
    }

    private void EndGame(bool win)
    {
        var message = win ? "Congratulations!" : "Game over, man!";
        _input.Disable();
        WinLoseText.text = message;
    }

    private void StoppedMoving()
    {
        _moving = false;
        if (_log != null)
        {
            _logOffset = _log.transform.position.x - transform.position.x;
            return;
        };
        CheckWin();
        CheckDeath();
    }

    private void CheckWin()
    {
        if (_win) EndGame(true);
    }
    private void CheckDeath()
    {
        if (_onRiver && _log == null) EndGame(false);
    }
}
