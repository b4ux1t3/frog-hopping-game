using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private Spawnable _spawnable;

    public string enemyName;
    // Start is called before the first frame update
    private void Awake()
    {
        _spawnable = GetComponent<Spawnable>();
        _spawnable.name = enemyName;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
