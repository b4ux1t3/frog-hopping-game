using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Serialization;

/// <summary>
/// All objects which will move on our "grid" system will get this class.
///
/// Movements are handled by simple inputs (up, down, left, right). This controller will decide how much world space to
/// move and in what direction.
/// </summary>
public class MovementController : MonoBehaviour
{
    public Vector3 moveSpeed;
    public float hopTime;

    private Vector3 _moveBeginning;
    private Vector3 _destination;
    private float _moveStartTime;
    private float _travelDistance;
    private bool _moving;
    private bool _broadcast;

    // Start is called before the first frame update
    private void Start()
    {
    }

    private void OnDisable()
    {
        _moveStartTime = 0;
        _travelDistance = 0;
        _moving = false;
        _moveBeginning = Vector3.zero;
        _destination = Vector3.zero;
    }

    // Update is called once per frame
    private void Update()
    {
        CalcMove();
    }

    //TODO: Refactor this to just take the vector2 from input. We can rely on having an axis.
    public void Up()
    {
        if (!_moving)
        {
            StartMove(Vector3.Scale(Vector3.up, moveSpeed), true);
        }
    }

    public void Down()
    {
        if (!_moving)
        {
            StartMove(Vector3.Scale(Vector3.down, moveSpeed), true);
        }

    }

    public void Left()
    {
        if (!_moving)
        {
            StartMove(Vector3.Scale(Vector3.left, moveSpeed), true);
        }

    }

    public void Right()
    {
        if (!_moving)
        {
            StartMove(Vector3.Scale(Vector3.right, moveSpeed), true);
        }
    }

    public void Move(Vector3 direction)
    {
        if (!(direction.magnitude > 0)) return;
        if (!_moving) StartMove(direction);
    }

    public void ResetPos()
    {
        _destination = new Vector3(_moveBeginning.x, _moveBeginning.y, _moveBeginning.z);
        _moveBeginning = _destination;
    }
    private void StartMove(Vector3 moveVector, bool broadcast = false)
    {
        if (!(moveVector.magnitude > 0)) return;
        var pos = transform.position;
        // ReSharper disable once Unity.InefficientPropertyAccess
        var scale = transform.localScale;
        _moveBeginning = pos;
        _moving = true;
        _moveStartTime = Time.time;
        _destination = moveVector + pos;
        _travelDistance = Vector3.Distance(pos, _destination);
        if (moveVector.x != 0)
        {
            transform.localScale = new Vector3(
                moveVector.normalized.x * math.abs(scale.x),
                scale.y * 1,
                scale.z * 1
            );
        }

        _broadcast = broadcast;
    }

    private void CalcMove()
    {
        if (!_moving) return;
        var pos = transform.position;
        var distanceTraveled = (Time.time - _moveStartTime) * hopTime;
        transform.position = Vector3.Lerp(_moveBeginning, _destination, distanceTraveled / _travelDistance);
        if (Vector3.Distance(pos, _destination) != 0) return;
        if (_broadcast) BroadcastMessage("StoppedMoving");
        _moving = false;
    }
}
