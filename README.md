# Frog-Like Animal Hopping Game

A completely unfaithful, minimum-viable product of a classic video game.

## Why?

In an effort to actually _finish_ a game, I asked my wife, "What classic game should I make?" 

She replied "Frogger".

For greater context, I, a _professional_ software developer (whatever that means), have never actually finished writing a video game. All of my video games end up in some barely-functional MVP-like state. All of them. I've tried participating in game jams, I've tried just having them as ongoing side-projects. I can never seem to keep up with them and they end up abandoned! 

The problems:
* I suffer from _immense_ scope-creepitis. I start working on a feature to enable a feature to enable a feature. . .and lose track of the goal. 

    * To solve this problem, I decided to set myself a target of an _existing_ video game. That way, there is, by definition, a limited scope. There's also a finish line.

* The other problem is that I'm always trying to use some different stack. Be it Phaser 3 (with TypeScript, of course), Unity, Godot, Unreal, or just plain HTML 5, I always try to write a new game in a new language/engine/whatever.

    * I'm solving this problem by just settling on Unity. I write C# almost daily at work. I know how it works, and I know how it's used in Unity.

So there we go. This will be Frogger, but not Frogger. I am not looking at any open-source code or implementations of Frogger. I'm not referencing anything Frogger-specific except the actual game itself.

## What tools are you using?

### Art

Everything here is hand-drawn/illustrated by me, in GIMP and/or on Samsung PENUP on my phone. Feel free to open a MR with your own hand-drawn assets! All your art work is yours. I need to figure out how to get the license to reflect that. Maybe hold off until I do that.

### Code

I'm using a combination of JetBrains Rider (which has _amazing_ Unity support) and VS Code (which has _okay_ Unity support) to edit the code. You don't have to use these things to contribute if you don't want to.

### Unity

I'm making pretty extensive use of the Unity Editor. Because, well, Unity. You won't be able to work on this project without Unity. 

### CI/CD

GitLab! Thanks GitLab! I'm using the excellent and well-documented [Game-CI](https://game.ci/docs/gitlab/getting-started) project to build this project in GitLab CI/CD pipelines.